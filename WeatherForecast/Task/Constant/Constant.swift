//
//  Constant.swift
//  Task
//
//  Created by Deepak on 10/09/22.
//

import Foundation

///Variable: Theme Font
var THEME_FONT = FontName.Montserrat

var themeBlueColor = "#CEF0FF";

var APIKey = "522db6a157a748e2996212343221502"

var baseUrl = "https://api.weatherapi.com"

///Variable: service
var apiClient = ApiClient(authbaseURL: baseUrl)

let defaults = UserDefaults.standard

var APP_NAME = "WeatherApp"

var currentCity = "Chennai"

var locationUpdate = "LocationUpdate"


