//
//  ApiClient.swift
//  Task
//
//  Created by Deepak on 10/09/22.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper


///MARK: Gloabl Variable AlamoManager
var AlamoManager = Alamofire.Session.default

///MARK: ApiClient
class ApiClient : SessionDelegate, RequestRetrier, RequestAdapter {
    
    
    ///MARK: Delegate method for session request
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        let urlRequest = urlRequest
          completion(.success(urlRequest))
    }
    
    ///MARK: Delegate method retry attempt
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        completion(.retryWithDelay(5))
    }
    
    ///MARK: Delegate method to handle the url redirection
    override func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        completionHandler(request)
    }
    
    ///Variable: Service auth base url
    var baseUrl:String!
    
    ///MARK: initialisation
    init() {
        
    }
    
    ///MARK: initialise with base url
    init(authbaseURL:String) {
        self.baseUrl = authbaseURL
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        configuration.headers = HTTPHeaders.default
    }
    
    ///MARK: Delegate method to process the request redirection url
    func task(_ task: URLSessionTask, willBeRedirectedTo request: URLRequest, for response: HTTPURLResponse, completion: @escaping (URLRequest?) -> Void) {
        print("redirect url",request.url)
    }
    
    ///MARK: Cancel all the request in queue for the current session
    func cancelAllRequests(){
        Alamofire.Session.default.session.getAllTasks(completionHandler: {tasks in tasks.forEach({ $0.cancel() })})
    }
    
    ///fetchData
    func fetchData(for city:String, completion:@escaping (_ response: Weather?) -> ()) {
        let weatherURl = baseUrl + ("/v1/forecast.json?key=\(APIKey)&q=\(city)&days=7&aqi=no&alerts=no".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
        let requestURL =  URL(string: weatherURl)!
        print("requestURL", requestURL.absoluteString)
        DispatchQueue.global().async {

            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default).validate(statusCode: 200...510).responseObject {(response: AFDataResponse<Weather>) in
                    switch response.result{
                    case .success(_):
                         completion(response.value)
//                        if let data = response.data{
//                            let jsonString = String(data: data, encoding: .utf8)
//                            print("success response",jsonString)
//                        }
                    case .failure(_):
                        if let data = response.data{
                            let jsonString = String(data: data, encoding: .utf8)
                            print("stop error",jsonString)
                        }
                        completion(response.value)
                    }

            }
        }
    }
}
