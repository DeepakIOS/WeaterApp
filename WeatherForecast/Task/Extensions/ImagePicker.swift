//
//  ImagePicker.swift
//  Task
//
//  Created by Deepak on 22/03/21.
//

import UIKit
import Foundation
import AVFoundation
import Photos
import MobileCoreServices

public protocol ImagePickerDelegate: class {
    func didSelect(image: UIImage?)
    func didSelect(file: URL?)
}

open class ImagePicker: NSObject {

    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    private weak var delegate: ImagePickerDelegate?

    public init(presentationController: UIViewController, delegate: ImagePickerDelegate) {
        self.pickerController = UIImagePickerController()

        super.init()

        self.presentationController = presentationController
        self.delegate = delegate

        self.pickerController.delegate = self
        self.pickerController.allowsEditing = false
        self.pickerController.mediaTypes = ["public.image"]
    }

    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }

        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.pickerController.sourceType = type
            self.presentationController?.present(self.pickerController, animated: true)
        }
    }
    
    public func openPhotos(){
//        let alert = UIAlertController(
//            title: "",
//            message: "Choose Option",
//            preferredStyle: UIAlertController.Style.actionSheet
//        )
//        alert.addAction(UIAlertAction(title: "Open Photos", style: .default, handler: { _ in
            self.pickerController.sourceType = .photoLibrary
            self.presentationController?.present(self.pickerController, animated: true)
//        }))
//        alert.addAction(UIAlertAction(title: "Open Files", style: .default, handler: { (alert) -> Void in
//            self.openDocument()
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        if UIDevice.current.userInterfaceIdiom == .pad {
//            alert.popoverPresentationController?.sourceView = presentationController?.view
//            alert.popoverPresentationController?.sourceRect = presentationController?.view.bounds ?? CGRect.zero
//            alert.popoverPresentationController?.permittedArrowDirections = [.down, .up]
//        }
//        self.presentationController?.present(alert, animated: true, completion: nil)

    }
    
    public func openDocument(){
        let types = [kUTTypePDF as String, kUTTypeText as String, kUTTypeRTF as String, kUTTypeSpreadsheet as String, kUTTypePNG as String, kUTTypeJPEG as String, "com.microsoft.word.doc", "org.openxmlformats.wordprocessingml.document"] as [String]
        let importMenu = UIDocumentPickerViewController(documentTypes: types, in: .import)
        
        if #available(iOS 11.0, *) {
            importMenu.allowsMultipleSelection = false
        }
        
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        
        self.presentationController?.present(importMenu, animated: true)
    }
    
    public func openCamera(){
        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            if Platform.isSimulator{
                openPhotos()
            }else{
                let alertController = UIAlertController(title: nil, message: "Device has no camera", preferredStyle: .alert)

                let okAction = UIAlertAction(title: "Alright", style: .default, handler: { (alert: UIAlertAction!) in
                })

                alertController.addAction(okAction)
                self.presentationController?.present(alertController, animated: true, completion: nil)
            }
        }else{
            checkCamera()
        }
    }
    private func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            openPhotos()
        case .denied, .restricted :
            alertToEncouragePhotoAccessInitially()
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    self.openPhotos()
                case .denied, .restricted:
                    self.alertToEncouragePhotoAccessInitially()
                case .notDetermined:
                    self.alertToEncouragePhotoAccessInitially()
                case .limited:
                    self.openPhotos()
                @unknown default:
                    break
                }
            }
        case .limited:
            openPhotos()
        @unknown default:
            break
        }
    }
    private func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized: DispatchQueue.main.async {
            self.pickerController.sourceType = .camera
            self.presentationController?.present(self.pickerController, animated: true)
        }
        case .denied: DispatchQueue.main.async { self.alertToEncourageCameraAccessInitially() }
        case .notDetermined: AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
            if (granted) {
                DispatchQueue.main.async {
                    self.pickerController.sourceType = .camera
                    self.presentationController?.present(self.pickerController, animated: true)
                }
            } else { DispatchQueue.main.async {self.alertToEncourageCameraAccessInitially() } }
        }
        default: break
        }
    }
    
    func alertToEncouragePhotoAccessInitially() {
        let alert = UIAlertController(
            title: APP_NAME,
            message: "Photo Library access required for pick photos!",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { _ in
            
        }))
        alert.addAction(UIAlertAction(title: "Open Settings", style: .cancel, handler: { (alert) -> Void in
            guard let appSettingURl = URL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!) else { return }
            if UIApplication.shared.canOpenURL(appSettingURl) {
                    UIApplication.shared.open(appSettingURl, options: [:], completionHandler: nil)
            }
        }))
        self.presentationController?.present(alert, animated: true, completion: nil)
    }
    
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: APP_NAME,
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { _ in
            
        }))
        alert.addAction(UIAlertAction(title: "Open Settings", style: .cancel, handler: { (alert) -> Void in
            guard let appSettingURl = URL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!) else { return }
            if UIApplication.shared.canOpenURL(appSettingURl) {
                    UIApplication.shared.open(appSettingURl, options: [:], completionHandler: nil)
            }
        }))
        self.presentationController?.present(alert, animated: true, completion: nil)
    }

    public func present(from sourceView: UIView) {

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        if let action = self.action(for: .camera, title: "Take photo") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .photoLibrary, title: "Photo library") {
            alertController.addAction(action)
        }

        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }

        self.presentationController?.present(alertController, animated: true)
    }

    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)

        self.delegate?.didSelect(image: image)
    }
}

extension ImagePicker: UIImagePickerControllerDelegate {

    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.pickerController(picker, didSelect: nil)
    }

    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let image = info[.originalImage] as? UIImage else {
            return self.pickerController(picker, didSelect: nil)
        }
        self.pickerController(picker, didSelect: image)
    }
}

extension ImagePicker: UINavigationControllerDelegate {

}

extension ImagePicker: UIDocumentPickerDelegate {
//    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
//        guard let path = urls.first else {
//                return
//            }
//        print("doc path : \(path)")
//        self.delegate?.didSelect(file: path)
//    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
//        guard let path = urls.first else {
//                return
//            }
        self.delegate?.didSelect(file: url)
    }

    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
