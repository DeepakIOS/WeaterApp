//
//  LoadingIndicatorView.swift
//  CityEV
//
//  Created by Deepak on 27/04/20.
//  Copyright © 2020 mavin. All rights reserved.
//
import UIKit
import NVActivityIndicatorView
/// MARK:- LoadingIndicatorView
class LoadingIndicatorView {
    
    /// Current overlay view
    static var currentOverlay : UIView?
    
    /// Loading Timer
    static var loadingTimer:Timer?
    
    /// Show the loading indicator over the view
    static func show() {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            return
        }
        show(currentMainWindow)
    }
    
    /// Show the loading indicator over the view with the message
    static func show(_ loadingText: String) {
        DispatchQueue.main.async {
            guard let currentMainWindow = UIApplication.shared.keyWindow else {
                return
            }
            show(currentMainWindow, loadingText: loadingText)
        }
    }
    
    /// Show the loadint over the targer
    static func show(_ overlayTarget : UIView) {
        show(overlayTarget, loadingText: nil)
    }
    
    /// Show the loadint over the targer with the loading text
    static func show(_ overlayTarget : UIView, loadingText: String?) {
        hide()
    
        /// Set the bounds to the overlay from the target
        let overlay = UIView(frame: overlayTarget.bounds)
        overlay.tag = 100
        overlay.center = overlayTarget.center
        overlay.alpha = 1
        overlay.backgroundColor = overlayTarget.tag == 199 ? UIColor.clear : UIColor.black.withAlphaComponent(0.05)//.clear//THEME_GREEN.toUIColor()
        overlay.layer.cornerRadius = 6
        overlay.clipsToBounds = true
        overlayTarget.addSubview(overlay)
        
        /// Set the text over the overlay
        if let textString = loadingText {
            
            let overlayView = UIView(frame: CGRect(x: (overlayTarget.frame.width - 300)/2 , y: (overlayTarget.frame.height - 150)/2, width: 300, height:150))
            overlayView.clipsToBounds = true
            overlayView.layer.cornerRadius = 15
            overlayView.backgroundColor = themeBlueColor.toUIColor()
                   
            
            let label = UILabel()
            let textHeight = loadingText?.heightForView(font: UIFont.systemFont(ofSize: 14), width: overlayView.frame.width - 60)
            label.frame = CGRect(x: 30, y: 20, width: overlayView.frame.width - 60, height: textHeight ?? 50)
            label.tag = 102
            label.text = textString
            label.textColor = .black//THEME_GREEN.toUIColor()
            label.font = UIFont.systemFont(ofSize: 14)
            label.textAlignment = .center
            label.numberOfLines = 0
//            label.sizeToFit()
            //label.center = CGPoint(x: nvActivityIndicator.center.x, y: nvActivityIndicator.center.y + 60)
            overlayView.addSubview(label)
        
        //overlayView.bringSubviewToFront(overlay)
        
            let nvActivityIndicator = NVActivityIndicatorView(frame: CGRect(x: (overlayView.frame.width - 36)/2 , y: (textHeight ?? 50) + 45, width: 36, height:36), type: NVActivityIndicatorType.ballSpinFadeLoader, color: themeBlueColor.toUIColor(), padding: 0.0)
            nvActivityIndicator.tintColor = themeBlueColor.toUIColor()
//        /// Set the indicator bounds and dimensions
//        let indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
//        indicator.assignColor(THEME_GREEN.toUIColor())
//        indicator.tag = 101
//        indicator.frame = CGRect(x: (overlayTarget.frame.width - 80)/2 , y: (overlayTarget.frame.height - 80)/2, width: 80, height:80)
////        indicator.center = overlay.center
////        indicator.backgroundColor = THEME_GREEN.toUIColor()
//        indicator.layer.cornerRadius = 6
//        indicator.clipsToBounds = true
//        indicator.startAnimating()
//        overlay.addSubview(indicator)
//        overlay.bringSubviewToFront(indicator)
        
        nvActivityIndicator.startAnimating()
        overlayView.addSubview(nvActivityIndicator)
            
            overlay.addSubview(overlayView)
            overlay.bringSubviewToFront(overlayView)
        }else{
            let overlayView = UIView(frame: CGRect(x: (overlayTarget.frame.width - 80)/2 , y: (overlayTarget.frame.height - 80)/2, width: 80, height:80))
            overlayView.clipsToBounds = true
            overlayView.layer.cornerRadius = 15
            overlayView.backgroundColor = UIColor.white
            overlay.addSubview(overlayView)
            overlay.bringSubviewToFront(overlayView)
            
            let nvActivityIndicator = NVActivityIndicatorView(frame: CGRect(x: (overlayView.frame.width - 36)/2 , y: 22, width: 36, height:36), type: NVActivityIndicatorType.ballSpinFadeLoader, color: themeBlueColor.toUIColor(), padding: 0.0)
            nvActivityIndicator.startAnimating()
            nvActivityIndicator.tintColor = themeBlueColor.toUIColor()
            overlayView.addSubview(nvActivityIndicator)
        }
        //overlayView.bringSubviewToFront(nvActivityIndicator)
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
//        overlay.alpha = overlay.alpha > 0 ? 0 : 1
        UIView.commitAnimations()
        
        currentOverlay = overlay
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 30, execute: {
//            self.hide()
//        })
        
        loadingTimer?.invalidate()
        loadingTimer = nil

        var time = 30.0
        
//        if let lText = loadingText, lText == "Please wait while we authorize the \npayment card"{
//            time = 50.0
//        }
//
//        if let lText = loadingText, lText == "Please wait while we initiate the \ncharging session"{
//            time = 60.0
//        }
        loadingTimer = Timer.scheduledTimer(timeInterval: time, target: self, selector: #selector(self.hide), userInfo: nil, repeats: false)
    }
    
    /// Hide the loading indicator view
    @objc static func hide() {
        if currentOverlay != nil {
            //DispatchQueue.main.async {
                currentOverlay?.removeFromSuperview()
                currentOverlay =  nil
            //}
        }
    }
}
