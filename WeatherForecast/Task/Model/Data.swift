//
//  Data.swift
//  Task
//
//  Created by Deepak on 10/09/22.
//

import Foundation
import ObjectMapper


///Data Model: Weather Response
class Weather: Mappable, Codable {
    ///Variable: name
    var name = String()
    ///Variable: region
    var region = String()
    ///Variable: country
    var country = String()
    ///Variable: current temperature in celcius
    var current_temp_c = Double()
    ///Variable: current temperature in fahrenheit
    var current_temp_f = Double()
    ///Variable: country
    var forecast = [Forecast]()
    ///Variable: localtime
    var localtime = String()
    ///Variable: message
    var message = String()
    /// Initialize
    required init?(){

    }
    
    /// Initialize Mappable
    required init?(map: Map) {
        
    }
    
    ///Mark: Method to map the response to class vaiables
    func mapping(map: Map) {
        name <- map["location.name"]
        region <- map["location.region"]
        country <- map["location.country"]
        current_temp_c <- map["current.temp_c"]
        current_temp_f <- map["current.temp_f"]
        forecast <- map["forecast.forecastday"]
        localtime <- map["localtime"]
        message <- map["error.message"]
    }
    
}

///Data Model: Forecast Response
class Forecast: Mappable, Codable {
    ///Variable: date
    var date = String()
    ///Variable: max temperature in celcius
    var maxtemp_c = Double()
    ///Variable: max temperature in fahrenheit
    var maxtemp_f = Double()
    ///Variable: min temperature in celcius
    var mintemp_c = Double()
    ///Variable: min temperature in fahrenheit
    var mintemp_f = Double()
    ///Variable: weather text
    var weather_text = String()
    ///Variable: icon
    var icon = String()
    /// Initialize
    required init?(){

    }
    
    /// Initialize Mappable
    required init?(map: Map) {
        
    }
    
    ///Mark: Method to map the response to class vaiables
    func mapping(map: Map) {
        date <- map["date"]
        maxtemp_c <- map["day.maxtemp_c"]
        maxtemp_f <- map["day.maxtemp_f"]
        mintemp_c <- map["day.mintemp_c"]
        mintemp_f <- map["day.mintemp_f"]
        weather_text <- map["day.condition.text"]
        icon <- map["day.condition.icon"]
    }
    
}
