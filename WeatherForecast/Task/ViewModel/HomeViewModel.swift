//
//  HomeViewModel.swift
//  Task
//
//  Created by Deepak on 10/09/22.
//

import Foundation
import UIKit

///MARK: Home View Model
class HomeViewModel: NSObject {
    ///variable:  Login model object
    static let sharedInstance = HomeViewModel()
    
    var weather = Weather(){
        didSet {
            reloadTableView?()
        }
    }
    
    var reloadTableView: (() -> Void)?
    
    func setupDefaultButton(button:UIButton){
        button.setTitle("", for: .normal)
        button.setTitle("", for: .selected)
        button.setTitle("", for: .highlighted)
    }
    
    ///MARK: model for login
    func fetchData(for city:String, completion : @escaping (_ error: String, _ sucess: Bool,_ response: Weather?)-> Void){
        LoadingIndicatorView.show()
        apiClient.fetchData(for: city, completion: {(response) in
            LoadingIndicatorView.hide()
//            if let error = response?.errordetails, error.count > 0{
//                print(error)
//                completion(error, false, (response ?? nil)!)
//            }else{
                completion("Success", true,response)
//            }
        })
    }
    
    func getWeather(for city:String) {
        self.fetchData(for: city) { success, status, response in
            if status{
                self.weather = response
            } else {
                print(response)
            }
        }
    }
}
