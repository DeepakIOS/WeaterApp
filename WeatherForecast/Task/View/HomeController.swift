//
//  HomeController.swift
//  WeatherApp
//
//  Created by Deepak on 10/09/22.
//

import UIKit
import Kingfisher

class HomeController: UIViewController {
    
    lazy var weatherBackground: UIImageView = {
        let weatherBackground = UIImageView()
        weatherBackground.translatesAutoresizingMaskIntoConstraints = false
        weatherBackground.contentMode = .scaleAspectFill
        return weatherBackground
    }()
    
    lazy var cityLabel: UILabel = {
        let cityLabel = UILabel()
        cityLabel.translatesAutoresizingMaskIntoConstraints = false
        cityLabel.textColor = .black
        cityLabel.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        cityLabel.text = ""
        cityLabel.adjustsFontSizeToFitWidth = true
        cityLabel.textAlignment = .center
        return cityLabel
    }()
    
    lazy var cityTemperature: UILabel = {
        let cityTemperature = UILabel()
        cityTemperature.translatesAutoresizingMaskIntoConstraints = false
        cityTemperature.textColor = .black
        cityTemperature.font = UIFont.systemFont(ofSize: 50, weight: .semibold)
        cityTemperature.text = ""
        cityTemperature.sizeToFit()
        return cityTemperature
    }()
    
    lazy var weatherText: UILabel = {
        let weatherText = UILabel()
        weatherText.translatesAutoresizingMaskIntoConstraints = false
        weatherText.textColor = .black
        weatherText.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        weatherText.text = ""
        weatherText.sizeToFit()
        return weatherText
    }()
    
    lazy var tempRange: UILabel = {
        let tempRange = UILabel()
        tempRange.translatesAutoresizingMaskIntoConstraints = false
        tempRange.textColor = .black
        tempRange.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        tempRange.text = ""
        tempRange.sizeToFit()
        return tempRange
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 44
        tableView.register(ForecastCell.self, forCellReuseIdentifier: "ForecastCell")
        tableView.layer.cornerRadius = 12
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    lazy var segmentControl: UISegmentedControl = {
        let items = ["°C", "°F"]
        let segmentControl = UISegmentedControl(items: items)
        segmentControl.translatesAutoresizingMaskIntoConstraints = false
        segmentControl.selectedSegmentIndex = 0
        segmentControl.addTarget(self, action: #selector(self.segmentValueChanged(_:)), for: .valueChanged)
        return segmentControl
    }()
    
    lazy var locationButton: UIButton = {
        let locationButton = UIButton(type: .custom)
        locationButton.translatesAutoresizingMaskIntoConstraints = false
        locationButton.addTarget(self, action: #selector(self.locationTapped(_:)), for: .touchUpInside)
        locationButton.setTitle("", for: .normal)
        locationButton.setTitle("", for: .selected)
        locationButton.setTitle("", for: .highlighted)
        locationButton.isUserInteractionEnabled = true
        locationButton.layer.cornerRadius = 8
        locationButton.backgroundColor = .white
        locationButton.setImage(UIImage(named: "map_location"), for: .normal)
        locationButton.setImage(UIImage(named: "map_location"), for: .selected)
        locationButton.setImage(UIImage(named: "map_location"), for: .highlighted)
        locationButton.imageView?.contentMode = .scaleAspectFit
        return locationButton
    }()
    
    lazy var loadingView: UIView = {
        let loadingView = UIView()
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.backgroundColor = "#CEF0FF".toUIColor()
        return loadingView
    }()
    
    lazy var loadingImage: UIImageView = {
        let loadingImage = UIImageView()
        loadingImage.translatesAutoresizingMaskIntoConstraints = false
        loadingImage.contentMode = .scaleAspectFit
        loadingImage.image = UIImage(named: "logo")
        return loadingImage
    }()
    
    lazy var loadingLabel: UILabel = {
        let loadingLabel = UILabel()
        loadingLabel.translatesAutoresizingMaskIntoConstraints = false
        loadingLabel.textColor = .black
        loadingLabel.font = UIFont.systemFont(ofSize: 40, weight: .semibold)
        loadingLabel.text = "Processing..."
        loadingLabel.sizeToFit()
        return loadingLabel
    }()
    
    lazy var currentDateTime: UILabel = {
        let currentDateTime = UILabel()
        currentDateTime.translatesAutoresizingMaskIntoConstraints = false
        currentDateTime.textColor = .black
        currentDateTime.font = UIFont.systemFont(ofSize: 16)
        currentDateTime.text = "Date & Time"
        currentDateTime.sizeToFit()
        return currentDateTime
    }()
    
    lazy var viewModel = {
        HomeViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(weatherBackground)
        view.sendSubviewToBack(weatherBackground)
        
        view.addSubview(segmentControl)
        view.addSubview(locationButton)
        view.addSubview(currentDateTime)
        
        view.addSubview(cityLabel)
        view.addSubview(cityTemperature)
        view.addSubview(weatherText)
        view.addSubview(tempRange)
        
        view.addSubview(tableView)
        view.addSubview(loadingView)
        
        loadingView.addSubview(loadingImage)
        loadingView.addSubview(loadingLabel)
        
        showLoading()
        
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        } else {
            // Fallback on earlier versions
        }
        
        initViewModel()
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshControlOpen), name: NSNotification.Name("LocationChanged"), object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupUI()
    }
    
    func setupUI() {
        weatherBackground.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        weatherBackground.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        segmentControl.leftAnchor.constraint(equalTo: weatherBackground.leftAnchor, constant: 20).isActive = true
        segmentControl.widthAnchor.constraint(equalToConstant: 100).isActive = true
        segmentControl.topAnchor.constraint(equalTo: weatherBackground.topAnchor, constant: 50).isActive = true
        
        locationButton.rightAnchor.constraint(equalTo: weatherBackground.rightAnchor, constant: -20).isActive = true
        locationButton.widthAnchor.constraint(equalToConstant: 32).isActive = true
        locationButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        locationButton.topAnchor.constraint(equalTo: weatherBackground.topAnchor, constant: 50).isActive = true
        
        currentDateTime.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        currentDateTime.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        
        cityLabel.bottomAnchor.constraint(equalTo: cityTemperature.topAnchor, constant: -4).isActive = true
        cityLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        cityLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        cityLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true

        cityTemperature.bottomAnchor.constraint(equalTo: weatherText.topAnchor, constant: -4).isActive = true
        cityTemperature.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        weatherText.bottomAnchor.constraint(equalTo: tempRange.topAnchor, constant: -4).isActive = true
        weatherText.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        tempRange.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -40).isActive = true
        tempRange.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: tempRange.bottomAnchor, constant: 8).isActive = true
//        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: UIDevice.current.orientation.isLandscape ? 80 : 20).isActive = true
        //tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: UIDevice.current.orientation.isLandscape ? -80 : -20).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true
        tableView.heightAnchor.constraint(equalToConstant: 172).isActive = true
        
        loadingView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        loadingView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        loadingImage.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        loadingImage.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        loadingImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loadingImage.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        loadingLabel.bottomAnchor.constraint(equalTo: loadingImage.topAnchor, constant: -40).isActive = true
        loadingLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func showLoading(){
        view.bringSubviewToFront(loadingView)
        loadingView.isHidden = false
    }
    
    /// Mark:- refreshControlOpen
    @objc func refreshControlOpen(){
        viewModel.getWeather(for: currentCity)
    }
    
    func initViewModel() {
        // Get articles data
        viewModel.getWeather(for: currentCity)
        
        viewModel.setupDefaultButton(button: locationButton)
        
        // Reload TableView closure
        viewModel.reloadTableView = { [weak self] in
            
            self?.setWeaterData()
            DispatchQueue.main.async {
                self?.view.sendSubviewToBack(self?.loadingView ?? UIView())
                self?.loadingView.isHidden = true
                
                self?.tableView.reloadData()
            }
            
            if !(self?.viewModel.weather?.message ?? "").isEmpty{
                self?.showAlert(withTitle: self?.viewModel.weather?.message ?? "Sorry unable to fetch data at this moment, please try again later")
            }
        }
    }
    
    @objc func segmentValueChanged(_ sender: UISegmentedControl) {
        setWeaterData()
    }
    
    @objc func locationTapped(_ sender:UIButton){
        let locationVC = LocationController()
        self.present(locationVC, animated: true, completion: nil)
    }
    
    func setWeaterData(){
        if segmentControl.selectedSegmentIndex == 0{
            setWeaterDataForCelcius()
        }else{
            setWeaterDataForFahrenheit()
        }
        setWeaterBackground()
        tableView.reloadData()
    }
    
    func setWeaterDataForCelcius(){
        DispatchQueue.main.async {
            if !(self.viewModel.weather?.name ?? "").isEmpty{
                self.cityLabel.text = (self.viewModel.weather?.name ?? "") + ", " + (self.viewModel.weather?.country ?? "")
            }
            self.cityTemperature.text = (self.viewModel.weather?.current_temp_c ?? 30).roundTo(places: 0).toInt().toString() + "°"
            self.weatherText.text = self.viewModel.weather?.forecast.first?.weather_text ?? "Clear Sky"
            
            let lowTemp = "L:" + ((self.viewModel.weather?.forecast.first?.mintemp_c.roundTo(places: 0).toInt() ?? 30).toString() + "°")
            let highTemp = "H:" + ((self.viewModel.weather?.forecast.first?.maxtemp_c.roundTo(places: 0).toInt() ?? 30).toString() + "°") + ""
            self.tempRange.text = lowTemp + " " + highTemp
            
            self.currentDateTime.text = Date().toStringFormat(format: "MMMM dd yyyy HH:mm")
        }
    }
    
    func setWeaterDataForFahrenheit(){
        DispatchQueue.main.async {
            self.cityLabel.text = self.viewModel.weather?.name
            self.cityTemperature.text = (self.viewModel.weather?.current_temp_f ?? 30).roundTo(places: 0).toInt().toString() + "°"
            self.weatherText.text = self.viewModel.weather?.forecast.first?.weather_text ?? "Clear Sky"
            
            let lowTemp = "L:" + ((self.viewModel.weather?.forecast.first?.mintemp_f.roundTo(places: 0).toInt() ?? 30).toString() + "°")
            let highTemp = "H:" + ((self.viewModel.weather?.forecast.first?.maxtemp_f.roundTo(places: 0).toInt() ?? 30).toString() + "°") + ""
            self.tempRange.text = lowTemp + " " + highTemp
        }
    }
    
    func setWeaterBackground(){
        let current_temp =  (self.viewModel.weather?.current_temp_c ?? 0).roundTo(places: 0).toInt()
        
        switch current_temp{
            case 0...20:
                DispatchQueue.main.async {
                    self.weatherBackground.image = UIImage(named: "rain")
                }
                break
            case 20...25:
                DispatchQueue.main.async {
                    self.weatherBackground.image = UIImage(named: "cloud")
                }
                break
            case 25...30:
                DispatchQueue.main.async {
                    self.weatherBackground.image = UIImage(named: "sunny")
                }
                break
            case 30...50:
                DispatchQueue.main.async {
                    self.weatherBackground.image = UIImage(named: "hot")
                }
                break
            default:
                DispatchQueue.main.async {
                    self.weatherBackground.image = UIImage(named: "sunny")
                }
                break
        }
    }

}

extension HomeController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.weather?.forecast.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        let calendarImage = UIImageView(frame: CGRect(x: 20, y: 9, width: 22, height: 22))
        calendarImage.image = UIImage(named: "calendar")
        headerView.addSubview(calendarImage)
        
        let titleLabel = UILabel(frame: CGRect(x: 48, y: 0, width: 200, height: 40))
        titleLabel.text = (viewModel.weather?.forecast.count ?? 0).toString() + "-DAY FORECAST"
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        titleLabel.textColor = .gray
        headerView.addSubview(titleLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell") as? ForecastCell{
            if let foreCast = viewModel.weather?.forecast[indexPath.row]{
                cell.parent = self
                cell.configureCell(forecast: foreCast)
            }
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return ((viewModel.weather?.forecast.count ?? 0) == 0) ? 0 : 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

