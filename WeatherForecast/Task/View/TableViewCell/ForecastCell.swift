//
//  HomeCell.swift
//  WeatherApp
//
//  Created by Deepak on 10/09/22.
//

import Foundation
import UIKit
import Kingfisher

class ForecastCell:UITableViewCell{
    
    lazy var dayLabel: UILabel = {
        let dayLabel = UILabel()
        dayLabel.translatesAutoresizingMaskIntoConstraints = false
        dayLabel.textColor = .black
        dayLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        dayLabel.text = ""
        dayLabel.sizeToFit()
        return dayLabel
    }()
    
    lazy var weatherImageView: UIImageView = {
        let weatherImageView = UIImageView()
        weatherImageView.translatesAutoresizingMaskIntoConstraints = false
        weatherImageView.contentMode = .scaleAspectFill
        return weatherImageView
    }()
    
    lazy var minTemperature: UILabel = {
        let minTemperature = UILabel()
        minTemperature.translatesAutoresizingMaskIntoConstraints = false
        minTemperature.textColor = .black
        minTemperature.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        minTemperature.text = ""
        minTemperature.sizeToFit()
        minTemperature.textAlignment = .left
        return minTemperature
    }()
    
    lazy var maxTemperature: UILabel = {
        let maxTemperature = UILabel()
        maxTemperature.translatesAutoresizingMaskIntoConstraints = false
        maxTemperature.textColor = .black
        maxTemperature.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        maxTemperature.text = ""
        maxTemperature.sizeToFit()
        maxTemperature.textAlignment = .left
        return maxTemperature
    }()
    
    var parent:HomeController!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCellView() {
        contentView.backgroundColor = UIColor.white.withAlphaComponent(0.9)
        backgroundColor = .clear
        
        addSubview(dayLabel)
        addSubview(weatherImageView)
        addSubview(minTemperature)
        addSubview(maxTemperature)
        
        dayLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        dayLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        
        weatherImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        weatherImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 90).isActive = true
        weatherImageView.widthAnchor.constraint(equalToConstant: 44).isActive = true
        weatherImageView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        minTemperature.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        minTemperature.widthAnchor.constraint(equalToConstant: 55).isActive = true
        minTemperature.trailingAnchor.constraint(equalTo: maxTemperature.leadingAnchor, constant: -16).isActive = true
        
        maxTemperature.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        maxTemperature.widthAnchor.constraint(equalToConstant: 55).isActive = true
        maxTemperature.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8).isActive = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    
    func setupUI() {
    }
    
    func configureCell(forecast:Forecast){
        dayLabel.text = forecast.date.toCorrectDate().toStringFormat(format: "EEE")
        
        if let url = URL(string: "https:" + forecast.icon){
            weatherImageView.kf.indicatorType = .activity
            weatherImageView.kf.cancelDownloadTask()
            weatherImageView.kf.setImage(with: url)
        }
        
        if parent.segmentControl.selectedSegmentIndex == 0{
            DispatchQueue.main.async {
                self.minTemperature.text = "L:" + forecast.mintemp_c.roundTo(places: 0).toInt().toString() + "°"
                self.maxTemperature.text = "H:" + forecast.maxtemp_c.roundTo(places: 0).toInt().toString() + "°"
            }
        }else{
            DispatchQueue.main.async {
                self.minTemperature.text = "L:" + forecast.mintemp_f.roundTo(places: 0).toInt().toString() + "°"
                self.maxTemperature.text = "H:" + forecast.maxtemp_f.roundTo(places: 0).toInt().toString() + "°"
            }
        }
    }
    
}
