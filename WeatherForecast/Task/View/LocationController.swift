//
//  LocationController.swift
//  WeatherApp
//
//  Created by Deepak on 15/09/22.
//

import UIKit
import MapKit

class LocationController: UIViewController, MKMapViewDelegate {

    lazy var selectButton: UIButton = {
        let locationButton = UIButton(type: .custom)
        locationButton.translatesAutoresizingMaskIntoConstraints = false
        locationButton.addTarget(self, action: #selector(self.selectButton(_:)), for: .touchUpInside)
        locationButton.setTitle("Select", for: .normal)
        locationButton.setTitle("Select", for: .selected)
        locationButton.setTitle("Select", for: .highlighted)
        locationButton.isUserInteractionEnabled = true
        locationButton.layer.cornerRadius = 20
        locationButton.backgroundColor = .black
        return locationButton
    }()
    
    lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.delegate = self
        mapView.zoomLevel = 20
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.showsUserLocation = true
        return mapView
    }()
    
    lazy var pinImageView: UIImageView = {
        let pinImageView = UIImageView()
        pinImageView.translatesAutoresizingMaskIntoConstraints = false
        pinImageView.contentMode = .scaleAspectFit
        pinImageView.image = UIImage(named: "map_pin")
        return pinImageView
    }()
    
    var locationManager = CLLocationManager()
    
    var ramdomString = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(mapView)
        mapView.addSubview(selectButton)
        mapView.addSubview(pinImageView)
        
        ramdomString = radNumber4Dig().toString()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupUI()
    }
    
    func setupUI() {
        view.backgroundColor = .red
        
        mapView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        mapView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        selectButton.widthAnchor.constraint(equalToConstant: 120).isActive = true
        selectButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        selectButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
        selectButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        pinImageView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        pinImageView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        pinImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pinImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
 

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.selectButton.isEnabled = false
        let center = mapView.centerCoordinate
        let address = CLGeocoder.init()
        address.reverseGeocodeLocation(CLLocation.init(latitude: center.latitude, longitude:center.longitude)) { (places, error) in
            if error == nil{
                if let place = places, let nPlace = place.first, let city = nPlace.locality{
                    self.selectButton.isEnabled = true
                    currentCity = city
                }else{
                    self.showToast(message: "Could't find places for this location", font: UIFont.systemFont(ofSize: 14))
                }
            }else{
                self.showToast(message: "Could't find places for this location", font: UIFont.systemFont(ofSize: 14))
            }
        }
    }
    
    @objc func selectButton(_ sender: UIButton) {
        NotificationCenter.default.post(name:Notification.Name.init("LocationChanged"), object: nil)
        self.dismiss(animated: false)
    }
}

extension LocationController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            print("not determined - hence ask for Permission")
            manager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            print("permission denied")
        case .authorizedAlways, .authorizedWhenInUse:
            print("Apple delegate gives the call back here once user taps Allow option, Make sure delegate is set to self")
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
        @unknown default:
            print("unknown state")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            DispatchQueue.once(token: ramdomString, block: {
                let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                self.mapView.setRegion(region, animated: true)
            })
        }
    }
}
